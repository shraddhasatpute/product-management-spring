package org.shraddha.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Product 
{
public String productName;
public Double price;
public Integer quantity;

@Autowired //available only for Global Vars
public Category category;

@Autowired //available only for Global Vars
public Variations  variations;//spring

public void printProductDetails()
{
	System.out.println("Product [productName="+productName+",price="+price+",quantity="+quantity+"];Category["+category+"];Variations["+variations+"]");
}

}
