package org.shraddha;

import org.shraddha.data.Product;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication // Spring:Java this is Spring Application ...okay|| java: Ok
public class ApplicationMain 
{
	
	public static void main(String [] args)	
	{
		//Product product=new Product();
		ConfigurableApplicationContext context=SpringApplication.run(ApplicationMain.class,args);//Java:Spring run this Application under your empire
		//Object=>Bean
		Product product=context.getBean(Product.class);	
		
		//JVM(Core,JVM,RAM,JVM)
		product.productName="Pen";
		product.price=(double)5;
		product.quantity=100;
		
		product.category.name="Stationary";
		
		product.variations.variationList.add("Red Colour");
		product.variations.variationList.add("Blue Colour");
		product.variations.variationList.add("Black Colour");
		
		product.printProductDetails();
	
	}
	
	
}
